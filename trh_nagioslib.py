#!/usr/bin/env python2
# -*- coding: utf-8; indent-tabs-mode: nil -*-

# Copyright © 2018   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


"""Module for common Nagios plugin tasks, including SNMP functions.
"""


import re
import collections
import netsnmp
import subprocess
import optparse
import textwrap


# Cache of mappings from short names to fully qualified names.
__oid_longnames = { }

def oid_longname(oid):
    """Translate a (short) OID to its fully qualified version.

       E.g. "MIB-2.sysName" will be translated into
       ".iso.org.dod.internet.mgmt.mib-2.system.sysName"

       This currently works by running the snmptranslate(1) command, as the
       netsnmp Python module does not expose the needed functionality.  A
       cache is employed to speed up repeated translations of the same OID.
    """
    longname = __oid_longnames.get(oid)
    if not longname:
        p = subprocess.Popen(['snmptranslate', '-Of', oid],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output,errors = p.communicate()
        if not output:
            raise ValueError(errors.strip())
        longname = output.strip()
        __oid_longnames[oid] = longname

    return longname


def var_oid(var):
    """Return the OID of a Varbind variable, combining tag and iid.
       This is really only useful if UseLongNames or UseNumeric was set
       on the session when the variable was fetched.
    """
    oid = var.tag
    if var.iid != '':
        oid += '.' + var.iid
    return oid


def bulkwalk(session, root, bulksize=50):
    # TODO: Add support for 'inclusive' (snmpwalk -Ci equivalent)
    from netsnmp import VarList, Varbind as V
    assert session.UseLongNames

    root_longname = oid_longname(root)
    results = VarList()
    vars = VarList(V(root))
    # This does not work:
    ##if inclusive:
    ##    status = session.get(vars)
    ##    if ( status  and  status[0] is not None  and
    ##         var_oid(vars[0]) == root_longname ):
    ##        results.append(vars[0])
    root_longname += '.'
    while True:
        vars = VarList(vars[-1])
        status = session.getbulk(0, bulksize, vars)
        if not status:
            # FIXME: Better error handling?
            return results
        for v in vars:
            if not var_oid(v).startswith(root_longname):
                return results
            results.append(v)



def fetch_table(session, table, columns, bulksize=True):
    """Retrieve selected columns from an SNMP table.

       Returns an ordered dictionary of rows, the key being the index
       in the table, and the value being a named tuple of the requested
       columns.

       The table must currently be specified as <MIBNAME>::<TABLENAME>.
       Column names must be given as unqualified names, and must be
       resolvable as <MIBNAME>::<COLUMNNAME>.

       The bulksize parameter specifies how large chunks of each column
       should be fetched at a time (using GETBULK requests).  If set to
       zero (or False), normal GETNEXT requests wil be used instead.  As
       a special case, the value True means to use GETBULK with a default
       chunk size.

       If the table changed during the retreival of it, columns that
       did not exist for a particular row are set to None.  (This
       behaviour should probably be controllable.)

       All columns must belong to the same table, but this is currently
       not enforced.

       WARNING: The API is not set in stone yet.
    """
    mibname,tablename = table.split('::')
    tbl_longname = oid_longname(table)
    RowType = collections.namedtuple(
        (mibname + '__' + tablename).replace('-', '_'),
        columns)
    rows = collections.OrderedDict()
    if bulksize is True:
        bulksize = 50
    for colno,colname in enumerate(columns):
        if bulksize > 0:
            objs = bulkwalk(session, mibname + '::' + colname)
        else:
            objs = netsnmp.VarList(netsnmp.Varbind(mibname + '::' + colname))
            status = session.walk(objs)
        for v in objs:
            r = rows.get(v.iid)
            if not r:
                r = [None] * colno
            r.append(v.val)
            rows[v.iid] = r
    for iid,r in rows.items():
        rows[iid] = RowType(*(r + [None] * (len(columns) - len(r))))
    return rows



def ifname_to_ifindex(snmpsession, ifname, vendor=None):
    """Look up an interface name, returning its interface index.

       The name is looked up in the ifName column in IF-MIB::ifXTable,
       which is the name used in commands on the device itself, e.g.
       "xe-0/1/17.3", "Vlan23", "TenGigabitEthernet2/5", "A3" or "47",
       depending on the device OS.  Names are case sensitive, and must
       match exactly; abbrevations like several Network OS:es allow are
       not handled.

       If the name is a number surrounded by square brackets (e.g. "[17]"),
       the number is returned (as an int).

       As a special case, None is returned for the name "ANY", as well
       as for None.

       If the interface name is not found, LookupError is raised.
    """
    if ifname == "ANY" or ifname is None:
        return None
    if ifname.startswith('[') and ifname.endswith(']'):
        return int(ifname[1:-1])
    #
    # Traverse the ifXTable.ifName column looking for the interface name
    ifaces = fetch_table(snmpsession, 'IF-MIB::ifXTable', ['ifName'])
    for idx,(name,) in ifaces.items():
        if name == ifname:
            return int(idx)

    # Some OS:es don't show all interfaces in ifXTable.  Try searching
    # ifTable.ifDescr as well.
    # - Comware: subinterfaces (e.g. FortyGigE1/1/7.104) are not in ifXTable
    if vendor in ('comware',):
        ifaces2 = fetch_table(snmpsession, 'IF-MIB::ifTable', ['ifDescr'])
        for idx,(descr,) in ifaces2.items():
            if descr == ifname:
                return int(idx)

    raise LookupError("Interface name not found: %s" % (ifname,))



# Regular expressions for matching sysDescr strings to vendor/OS id tags.
#
# This constant is part of the public API, so users can get a list of
# supported OS identifiers.  Users should only assume that the elements
# are N-tuples, with the zeroth member in each tuple is the identifier.
# The identifiers "autodetect" and "none" are guaranteed to never be
# used, nor any identifiers consisting of only upper-case letters.
#
# Each element is a three-tuple <id,regexp,regexp-flags>.
# This is a list, not a dictionary, since:
#  - We want to process this in order, to e.g. be able to distinguish
#    between SmurfOS 17 and generic SmurfOS.  The regexp for SmurfOS 17
#    likely needs to come before the one matching generic SmurfOS.
#  - We might want to have multiple regexps for the same OS tag, in
#    case a vendor changes their sysDescr format radically
#  - A list of three-tuples is easier to use and process than a dict
#    mapping from identifiers to two-tuples
#
vendor_sysdescrs = [
    ('junos',       r"Juniper Networks.* JUNOS ", 0),
    ('procurve',    r"^ProCurve|Formerly ProCurve", 0),
    ('comware',     r"HP Comware", 0),
    ('dnos9',       r"Dell Networking OS.*Application Software Version: 9\.",
                    re.DOTALL),
    ('dnos6',       r"Dell Networking[^,]*,\s*6\.", 0),
    ('ios',         r"Cisco IOS Software", 0),
    ##('ios-xe',    ...Cisco IOS XE, 0),
    ##('ios-xr',    ...Cisco IOS-XR, 0),
    ##('nx-os',     ...Cisco Nexus NX-OS, 0),
]

def guess_vendor(sysDescr):
    """Try to guess vendor/OS based on the value of SNMPv2-MIB::sysDescr.

       The return value is a "symbol" string representing the vendor/OS
       to use in determining which quirks to work around.  Examples are
       'junos', 'procurve', 'comware' and 'dnos9'.  Returns None if the
       vendor/OS cannot be determined.

       The caller needs to fetch SNMPv2-MIB::sysDescr.0 from the device
       and pass in as the sysDescr parameter.
    """
    for vendorid,regexp,reflags in vendor_sysdescrs:
        if re.search(regexp, sysDescr, reflags):
            return vendorid
    return None



NAGIOSCODES = ['OK', 'WARNING', 'CRITICAL', 'UNKNOWN']


def nagios_report(statuses):
    """Format a Nagios status report and exit code.

       Takes a status dictionary (the STATUSES parameter), as produced
       e.g. by the check_ospf2_neighbour() function, and returns the proper
       exit code and a string to print.  The exit code will be the worst
       status found in STATUSES, and the string will be a concatenation of
       all status messages, in order of decreasing severity, each prepended
       by the symbolic status code.
    """
    max_level = -1
    message = ""
    # We want our messages with the most severe code first, so iterate
    # backwards over NAGIOSCODES.
    for lvl,st in reversed(list(enumerate(NAGIOSCODES))):
        for msg in statuses.get(st, []):
            if lvl >= max_level:
                max_level = lvl
            message += st + ": " + msg + "\n"

    if max_level < 0:
        max_level = NAGIOSCODES.index('UNKNOWN')
        message = "UNKNOWN: No status report\n"

    return max_level, message



class NoReflowHelpFormatter(optparse.IndentedHelpFormatter):
    """A HelpFormatter for optparse that does not re-wrap/reflow text.

       Intended for command descriptions that are already properly
       pre-formatted.
    """
    def format_description(self, description):
        if not description:
            return ""
        desc_width = min(70, self.width - self.current_indent)
        indent = " " * self.current_indent
        summary,body = (description.strip().split("\n", 1) + [""])[:2]
        body = textwrap.dedent(body)
        description = summary + "\n" + body + "\n"
        return description
