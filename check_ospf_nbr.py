#!/usr/bin/env python2
# -*- coding: utf-8; indent-tabs-mode: nil -*-

# Copyright © 2018   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


# Explicitly assign to __doc__ to avoid doc string being optimized out.
__doc__ = """\
Check OSPF neighbour adjacency status of a router using SNMP.
Neighbour can be specified by peer IP address, router-id, and/or interface
name, e.g. to check that the router has a neighbour with router-id R on
interface I.  The adjacency with the neighbour must be in state FULL for
it to be considered OK.

Either or both of OSPF v2 and OSPF v3 neighbours can be checked at the
same time.
"""

__version__ = '<#VERSION#>'


import sys
import re
import os
import optparse
import netsnmp
import ipaddr
import socket
import collections

import trh_nagioslib



def normalize_routerid(routerid):
    """Parse a router-id, converting it to a normalized dotted quad string.
       The router-id passed in can be an integer, an integer as a string,
       or a string in dotted quad ("10.20.30.40") format.
    """
    if routerid is None:
        return None

    try:
        try:
            idnum = int(routerid, 0)
        except TypeError:       # Handle idpsec being a numeric type
            idnum = int(routerid)
        # Continued in else clause
    except ValueError:
        m = re.match('^([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)$', routerid)
        if not m:
            raise ValueError("Illegal format for router-id: %s" % (routerid,))
        o1,o2,o3,o4 = map(int, m.groups())
        if o1 > 255 or o2 > 255 or o3 > 255 or o4 > 255:
            raise ValueError(
                "Octet in router-id out of range [0..255]: %d.%d.%d.%d" % (
                    o1, o2, o3, o4,))
    else:
        # Can't do this in the try clause, since we are raising ValueError
        if idnum < 0 or idnum > 0xffffffff:
            raise ValueError(
                "Router-id out of range [0..0xffffffff]: %s" % (idnum,))
        o1,o2,o3,o4 = ((idnum >> 24), (idnum >> 16) & 255,
                       (idnum >> 8) & 255, idnum & 255)

    idquad = '%d.%d.%d.%d' % (o1,o2,o3,o4)
    return idquad



class ProgramFailure(Exception):
    def __init__(self, status, msg):
        Exception.__init__(self)
        self.status = status
        self.msg = msg


class Options(optparse.OptionParser):

    @staticmethod
    def __routerid_option(option, opt_str, value, parser):
        try:
            rtrid = normalize_routerid(value)
        except ValueError as e:
            raise optparse.OptionValueError(str(e))
        setattr(parser.values, option.dest, rtrid)

    def __init__(self):
        global __doc__, __version__
        optparse.OptionParser.__init__(
            self,
            usage="%prog {-2|3} -H host [options] <neighbour-options>",
            version=__version__,
            description=__doc__)
        self.add_option(
            '-H', '--host', action='store', type='string',
            help=("Hostname/address of router to query; mandatory option"))
        self.add_option(
            '-C', '--community', action='store', type='string',
            default='public',
            help=("SNMP community [default: %default]"))
        self.add_option(
            '-c', action='store', type='string', dest='community',
            help=("Alias for -C/--community for backward compatibility with"
                  " older versions of this plugin"))
        self.add_option(
            '-v', '--snmp-version', action='store', type='choice',
            choices=['2',], default='2',
            help=("SNMP version to use [default: %default]."))
        self.add_option(
            '-N', '--neighbour-id', '--nbr', action='callback', type='string',
            callback=self.__routerid_option,
            help=("Router-id of neighbour, as a dotted quad, or as a decimal,"
                  " octal or hexadecimal integer"))
        self.add_option(
            '-P', '--peer-address', action='store', type='string',
            help=("IPv4 address of neighbour; only used for OSPFv2."
                  " This can be specified as a numeric address or as a"
                  " host name that will be resolved."))
        self.add_option(
            '-I', '--interface', action='store', type='string',
            help=("Layer 3 interface where neighbour should be found."
                  " [default: any]"))
        self.add_option(
            '-2', '--ospfv2', action='store_true', default=False,
            help=("Look for OSPFv2 neighbour [default: %default]"))
        self.add_option(
            '-3', '--ospfv3', action='store_true', default=False,
            help=("Look for OSPFv3 neighbour [default: %default]."))
        self.add_option(
            '--vendor', action='store', type='choice',
            choices=(['autodetect', 'none']
                     + sorted(set(v[0] for v in
                                  trh_nagioslib.vendor_sysdescrs ))),
            default='autodetect',
            help=("Handle quirks for specific network OS [default: %default]"))

    def get_version(self):
        progname = self.get_prog_name()
        pkgname = "<#PKGNAME#>"
        version = self.version
        vinfo = "%s (%s) version %s" % (progname, pkgname, version)
        return vinfo

    def check_values(self, values, args):
        if len(args) != 0:
            self.error("No arguments allowed")
        if not values.host:
            self.error("Host (-H option) must be specified")
        if not values.ospfv2 and not values.ospfv3:
            self.error("At least one OSPF version must be specified (-2, -3)")
        return values,args

    def exit(self, status=0, msg=None):
        if msg:
            sys.stderr.write(msg)
        # Exit with EX_USAGE, unless status==0 (which happens for --help)
        raise ProgramFailure(status=(status and os.EX_USAGE), msg=msg)


OPTIONS,_ = Options().parse_args(['-H', 'localhost', '-2'])


def fail(status, fmt, *args):
    progname = os.path.basename(sys.argv[0] or "check_ospf_nbr")
    msg = progname + ": " + fmt % args + "\n"
    sys.stderr.write(msg)
    raise ProgramFailure(status=status, msg=msg)



def interface_ipv4nets(snmpsession, ifindex, vendor=None):
    """Find all IPv4 networks on a specific interface.

       Returns a list of ipaddr.IPNetwork instances.
    """
    networks = []

    # First try the IP-MIB::ipAddrTable.  This is actually deprecated, but
    # some vendors don't have the more modern tables, or don't have the
    # information we want in them.
    ipAddrTable = trh_nagioslib.fetch_table(
        snmpsession, 'IP-MIB::ipAddrTable',
        ['ipAdEntAddr', 'ipAdEntNetMask', 'ipAdEntIfIndex'])
    for key,addrentry in ipAddrTable.items():
        if int(addrentry.ipAdEntIfIndex) == ifindex:
            net = ipaddr.IPNetwork(
                addrentry.ipAdEntAddr + '/' + addrentry.ipAdEntNetMask, 4)
            networks.append(net)

    # FIXME!  Look through other tables (IP-MIB::ipAddressTable).

    return networks


def ospf2_neighbours(snmpsession, vendor=None):
    """Find all OSPF v2 neighbours of device.
    """
    # For some reason, retreiving this particular table from a ProCurve
    # takes much longer using bulk fetching.
    dobulk = (vendor or OPTIONS.vendor) not in ['procurve']
    nbrTable = trh_nagioslib.fetch_table(
        snmpsession, 'OSPF-MIB::ospfNbrTable',
        [ 'ospfNbrIpAddr', 'ospfNbrAddressLessIndex', 'ospfNbrRtrId',
          'ospfNbrState',
        ],
        bulksize=(True if dobulk else 0))
    return nbrTable


def filter_ospf2_neighbours(nbrTable, snmpsession,
                            ifindex=None, router_id=None, peer_addr=None,
                            vendor=None):
    """Filter OSPF v2 neighbour table against search criteria.

       This takes an OSPFv2 neighbour table in dictionary form (as returned
       by the ospf2_neighbours() function), and returns those neighbours
       matching the search criteria of interface, router-id and/or peer IP
       address.

       Router-id and peer address are expected to be a string in normalized
       dotted quad format (e.g. "10.0.0.1", not "10.0.0.001" or 167772161).
       Interface index must be an integer.

       The value None acts as "don't care" for interface, router-id and peer
       address filtering.

       Both unnumbered links, where the interface index can be used directly
       to identify neighbours on that link, and normal numbered interfaces,
       where the IP address of the peer must be matched against the IPv4
       networks on that interface, are handled.  The IP networks are looked
       up using the SNMP session passed in.

       The return value will be of the same type as the table sent in,
       e.g. a normal dictionary or an ordered dictionary.
    """

    # Table of IPv4 networks for the specified interface; lazily fetched
    # when (and if) needed.
    if_v4networks = None

    matches = type(nbrTable)()          # Same type as original table
    for ix,nbr in nbrTable.items():
        if router_id is not None  and  nbr.ospfNbrRtrId != router_id:
            continue

        if peer_addr is not None  and  nbr.ospfNbrIpAddr != peer_addr:
            continue

        # Match against ifindex last, since that is potentially expensive
        # (may need to fetch address tables over SNMP).
        if ifindex is not None:
            nbr_if = int(nbr.ospfNbrAddressLessIndex)
            if nbr_if != 0:
                if nbr_if != ifindex:
                    continue
            else:
                if if_v4networks is None:
                    if_v4networks = interface_ipv4nets(
                        snmpsession, ifindex, vendor=vendor)
                # Note: This can give false positives when extruded subnets
                # are in use.
                for net in if_v4networks:
                    if ipaddr.IPAddress(nbr.ospfNbrIpAddr, 4) in net:
                        break
                else:
                    continue

        matches[ix] = nbr

    return matches



def check_ospf2_neighbour(snmpsession, statuses,
                          ifindex, router_id, peer_addr,
                          vendor=None):
    """Verify that an OSPFv2 neighbour exists and is in state 'full'.

       The neighbour is identified by the IFINDEX, ROUTER_ID and PEER_ADDR
       parameters, any or all of which can be None to signify "don't care".

       STATUSES is an in/out parameter, and must be a dictionary with
       symbolic Nagios status codes as keys, and lists of messages per
       code, but it need not be filled out.  The dictionary will be
       updated with status information about matching OSPFv2 neighbours;
       if no neighbours are found, a CRITICAL message to that effect will
       be added.

       The return value will be the STATUSES dictionary.  Thus it is OK to
       call this function as
           st = check_ospf2_neighbour(s, {}, <match-criteria...>)
    """
    all_nbrs = ospf2_neighbours(snmpsession)
    matching_nbrs = filter_ospf2_neighbours(
        all_nbrs, snmpsession, ifindex, router_id, peer_addr)
    for nbr in matching_nbrs.values():
        message = "OSPFv2 neighbour %s (address %s) state %s" % (
            nbr.ospfNbrRtrId, nbr.ospfNbrIpAddr, nbr.ospfNbrState)
        if nbr.ospfNbrState in ('full', '8',):
            statuses.setdefault('OK', []).append(message)
        else:
            statuses.setdefault('CRITICAL', []).append(message)

    if not matching_nbrs:
        statuses.setdefault('CRITICAL', []).append(
            "No matching OSPFv2 neighbour")

    return statuses



def ospf3_neighbours(snmpsession, vendor=None):
    """Find all OSPF v3 neighbours of device.
    """
    # For some reason, retreiving this particular table from a ProCurve
    # takes much longer using bulk fetching.
    dobulk = (vendor or OPTIONS.vendor) not in ['procurve']

    # The three index columns are not accessible, so we need to parse the iid
    # returned and pick them out ourselves.
    idxcols = [ 'ospfv3NbrIfIndex', 'ospfv3NbrIfInstId', 'ospfv3NbrRtrId', ]
    nbrcols = [ 'ospfv3NbrState', 'ospfv3NbrAddressType', 'ospfv3NbrAddress', ]
    RowType = collections.namedtuple('Ospfv3Nbr', (idxcols + nbrcols))
    nbrTable = trh_nagioslib.fetch_table(
        snmpsession, 'OSPFV3-MIB::ospfv3NbrTable', nbrcols,
        bulksize=(True if dobulk else 0))
    for idx,row in nbrTable.items():
        ifIdx,ifInst,nbrRtrId = idx.split('.')
        nbrRtrId = normalize_routerid(nbrRtrId)
        newrow = RowType(ifIdx, ifInst, nbrRtrId, *row)
        nbrTable[idx] = newrow

    return nbrTable


def filter_ospf3_neighbours(nbrTable, snmpsession,
                            ifindex=None, router_id=None,
                            vendor=None):
    """Filter OSPF v3 neighbour table against search criteria.

       This takes an OSPFv3 neighbour table in dictionary form (as returned
       by the ospf3_neighbours() function), and returns those neighbours
       matching the search criteria of interface and/or router-id.

       The router-id is expected to be a string in normalized dotted quad
       format (e.g. "10.0.0.1", not "10.0.0.001" or 167772161).  Interface
       index must be an integer.

       The value None acts as "don't care" for interface and router-id
       filtering.

       The return value will be of the same type as the table sent in,
       e.g. a normal dictionary or an ordered dictionary.
    """

    matches = type(nbrTable)()          # Same type as original table
    for ix,nbr in nbrTable.items():
        if router_id is not None  and  nbr.ospfv3NbrRtrId != router_id:
            continue

        if ifindex is not None:
            nbr_if = int(nbr.ospfv3NbrIfIndex)
            if nbr_if != 0:
                if nbr_if != ifindex:
                    continue

        matches[ix] = nbr

    return matches



def check_ospf3_neighbour(snmpsession, statuses,
                          ifindex, router_id,
                          vendor=None):
    """Verify that an OSPFv3 neighbour exists and is in state 'full'.

       The neighbour is identified by the IFINDEX and ROUTER_ID parameters,
       any or all of which can be None to signify "don't care".

       STATUSES is an in/out parameter, and must be a dictionary with
       symbolic Nagios status codes as keys, and lists of messages per
       code, but it need not be filled out.  The dictionary will be
       updated with status information about matching OSPFv3 neighbours;
       if no neighbours are found, a CRITICAL message to that effect will
       be added.

       The return value will be the STATUSES dictionary.  Thus it is OK to
       call this function as
           st = check_ospf3_neighbour(s, {}, <match-criteria...>)
    """
    OK_STATES = ('full', '8',)
    # Some versions of Junos (at least 20.3 and 20.4, but not 19.3 or earlier),
    # return 9 instead of 8 ('full') when the neighbour is in state full.
    if vendor == 'junos':
        OK_STATES += ('9',)

    all_nbrs = ospf3_neighbours(snmpsession)
    matching_nbrs = filter_ospf3_neighbours(
        all_nbrs, snmpsession, ifindex, router_id)
    for nbr in matching_nbrs.values():
        message = "OSPFv3 neighbour %s state %s" % (
            nbr.ospfv3NbrRtrId, nbr.ospfv3NbrState)
        if nbr.ospfv3NbrState in OK_STATES:
            statuses.setdefault('OK', []).append(message)
        else:
            statuses.setdefault('CRITICAL', []).append(message)

    if not matching_nbrs:
        statuses.setdefault('CRITICAL', []).append(
            "No matching OSPFv3 neighbour")

    return statuses



def main(argv):
    global OPTIONS
    OPTIONS, args = Options().parse_args(argv[1:])

    try:
        # net-snmp misparses some IPv6 addresses, believing the last part of
        # the address is a port number.  Work around that bug.
        snmphost = "udp6:[" + str(ipaddr.IPv6Address(OPTIONS.host)) + "]"
    except ipaddr.AddressValueError:
        snmphost = OPTIONS.host
    s = netsnmp.Session(DestHost=snmphost,
                        Version=int(OPTIONS.snmp_version),
                        Community=OPTIONS.community)
    s.UseLongNames = True
    s.UseEnums = True

    if OPTIONS.vendor == 'none':
        OPTIONS.vendor = None
    elif OPTIONS.vendor == 'autodetect':
        sysDescr, = s.get(netsnmp.VarList(
                netsnmp.Varbind('SNMPv2-MIB::sysDescr.0')))
        OPTIONS.vendor = trh_nagioslib.guess_vendor(sysDescr)

    try:
        ifindex = trh_nagioslib.ifname_to_ifindex(
            s, OPTIONS.interface, OPTIONS.vendor)
    except LookupError as e:
        fail(os.EX_DATAERR, str(e))

    peer_address = None
    if OPTIONS.peer_address is not None:
        try:
            peer_addrinfo = socket.getaddrinfo(
                OPTIONS.peer_address, None, socket.AF_INET)
        except socket.gaierror as e:
            fail(os.EX_NOHOST, "Bad peer address %s: %s",
                 OPTIONS.peer_address, str(e))
        for _fam,_socktype,_proto,_canon,(_ipaddr,_port) in peer_addrinfo:
            peer_address = _ipaddr
            break

    nbr_statuses = {}
    if OPTIONS.ospfv2:
        nbr_statuses = check_ospf2_neighbour(
            s, nbr_statuses, ifindex, OPTIONS.neighbour_id, peer_address,
            vendor=OPTIONS.vendor)
    if OPTIONS.ospfv3:
        nbr_statuses = check_ospf3_neighbour(
            s, nbr_statuses, ifindex, OPTIONS.neighbour_id,
            vendor=OPTIONS.vendor)

    lvl,message = trh_nagioslib.nagios_report(nbr_statuses)
    sys.stdout.write(message)
    return lvl



if __name__ == '__main__':
    try:
        code = main(sys.argv)
        sys.exit(code)
    except ProgramFailure as failure:
        sys.exit(failure.status)
    except Exception:
        # An exception would normally cause Python to exit with code == 1,
        # but that would be a WARNING for Nagios.  Avoid that.
        (exc_type, exc_value, exc_traceback) = sys.exc_info()
        import traceback
        traceback.print_exception(exc_type, exc_value, exc_traceback)
        sys.exit(os.EX_SOFTWARE)
