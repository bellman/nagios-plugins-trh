.DEFAULT_GOAL = all

#########################################################################
# Copyright © 2018-2019   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


#########################################################################
# Various programs and settings

# Nagios plugin directory; default is guessed further down
prefix		= /usr/local
plugin_subdir	= $(_plugin_subdir)
plugindir	= $(prefix)/$(plugin_subdir)

# Directory where MIB files are installed; default is guessed further down
mib_subdir	= $(_mib_subdir)
mibdir		= $(prefix)/$(mib_subdir)

binmode		= 555
libmode		= 444
datamode	= 444

PYTHON2		= python2
RM		= rm -vf
INSTALL		= install -c

include Version.inc


#########################################################################
# Internal variables, not intended to be overridden by users

_PYPLUGINS	= check_ospf_nbr check_ping_multiaddr
_PYLIBS		= trh_nagioslib
_MIBFILES	= OSPFV3-MIB.txt
_RPM_SPECFILE	= $(PKGNAME)-$(VERSION).spec

_PLUGINS	= $(_PYPLUGINS)
_PYLIB_SOURCES	= $(_PYLIBS:%=%.py)
_PYLIB_OBJS	= $(_PYLIBS:%=%.pyc) $(_PYLIBS:%=%.pyo)
_LIB_SOURCES	= $(_PYLIB_SOURCES)
_LIB_OBJS	= $(_PYLIB_OBJS)

_TARFILE	= $(PKGNAME)-$(VERSION).tar.gz

_ALL_SOURCES	:= $(shell git ls-files 2>/dev/null)

# Command to make substitutions of certain make variables
# Backslashes escape make comments, and prevents command from affecting itself.
_SUBSTITUTER	= sed  -e 's/<\#VERSION\#>/$(VERSION)/g' \
		       -e 's/<\#PKGNAME\#>/$(PKGNAME)/g'

# As above, but perform inline substitutions.
_SUBSTITUTER_I	= $(_SUBSTITUTER) -i


########################################################################
# Paths and other stuff guessed based on platform

_platform := $(shell uname -o):$(shell uname -p)

ifeq ($(_platform),GNU/Linux:x86_64)
  _plugin_subdir	= lib64/nagios/plugins
else ifeq ($(_platform),GNU/Linux:i386)
  _plugin_subdir	= lib/nagios/plugins
else
  _plugin_subdir	= lib/nagios/plugins
endif

_mib_subdir		= share/mibs/ietf


#########################################################################
# Public targets

.PHONY: all
all: _build

.PHONY: install
install: install-plugins install-mibs

.PHONY: install-plugins
install-plugins: _build
	$(INSTALL) -d $(DESTDIR)$(plugindir)
	$(INSTALL) -m$(binmode)  $(_PLUGINS)  $(DESTDIR)$(plugindir)
	$(INSTALL) -m$(libmode)  $(_LIB_SOURCES)  $(DESTDIR)$(plugindir)
	@sleep 1  # Make sure objs are newer than sources
	$(INSTALL) -m$(libmode)  $(_LIB_OBJS)  $(DESTDIR)$(plugindir)

.PHONY: install-mibs
install-mibs:
	$(INSTALL) -d $(DESTDIR)$(mibdir)
	$(INSTALL) -m$(datamode)  $(patsubst %,mibs/%,$(_MIBFILES)) \
	           $(DESTDIR)$(mibdir)

.PHONY: clean
clean:
	$(RM) $(_LIB_OBJS) $(_PYPLUGINS) $(_RPM_SPECFILE)
	$(RM) $(PKGNAME)-*.tar{,.gz,.bz2,.xz}
	$(RM) $(PKGNAME)-*.rpm
	$(RM) $(PKGNAME)-*.spec
	$(RM) *.pyc *.pyo *~


# Make a source release: create a tar archive of all files, with version
# information inserted.
.PHONY: srcrelease
srcrelease: _tarball
# Build a source RPM as part of the release process, if rpmbuild(8) is
# available.
ifeq ($(shell type rpmbuild >/dev/null 2>/dev/null; echo $$?),0)
srcrelease: srcrpm
endif
# And remind the user to set a Git tag and update Version.inc, if needed.
srcrelease:
	@if [ "$(_GITVERSION)" != "$(_RELVERSION)" ]; then \
	    echo; echo; echo; \
	    echo '        ************************************************'; \
	    echo '        *                                              *'; \
	    echo '        *  Remeber to update Version.inc and set tag!  *'; \
	    echo '        *                                              *'; \
	    echo '        ************************************************'; \
	    echo; echo; echo; \
	 fi


# Build a source RPM
.PHONY: srcrpm
srcrpm: $(_TARFILE)
# Defining a macro to another macro that does not exist, seems to be the
# most portable way of making it empty.
	rpmbuild -D "dist %{?__EMPTY__}" -D "_srcrpmdir `pwd`" -ts $(_TARFILE)



#########################################################################
# Internal targets

.PHONY: _build
_build: $(_PLUGINS) $(_LIB_OBJS) $(_RPM_SPECFILE)



$(_PYPLUGINS): %: %.py
	$(RM) "$@" ;  $(_SUBSTITUTER)  <"$<"  >"$@" ;  chmod +x "$@"

%.pyc: %.py
	$(PYTHON2) -c 'import py_compile; py_compile.compile("$<", "$@")'

%.pyo: %.py
	$(PYTHON2) -OO -c 'import py_compile; py_compile.compile("$<", "$@")'

$(_RPM_SPECFILE): pkg/rpm.spec.in
	$(RM) "$@" ;  $(_SUBSTITUTER)  <"$<"  >"$@"

.PHONY: _packaging
_packaging: $(_RPM_SPECFILE)


# Helper for the srcrelease target: create the actual tarball.
# The meat of it happens in _tarball_2.  That is split out, to have $_tmpdir
# (and $_dstdir) available as Make variables, but only run mktemp when we
# actually need it.
.PHONY: _tarball _tarball_2
_tarball $(_TARFILE): $(_ALL_SOURCES)
	_tmpdir=$$(mktemp -d -t "$(PKGNAME).`whoami`.XXXXXXXXXXXX"); \
	_dstdir="$${_tmpdir}/$(PKGNAME)-$(VERSION)"; \
	$(MAKE) _tarball_2  _tmpdir="$${_tmpdir}" _dstdir="$${_dstdir}"

_tarball_2:
	$(RM) "$(_TARFILE)"
	mkdir "$(_dstdir)"
	@#
	@# Create a copy of the source tree, so we can patch in version number
	@# and create tar archive from it.
	set -e; for f in $(_ALL_SOURCES); do \
	    d=`dirname "$$f"`  &&  mkdir -p "$(_dstdir)/$$d"  && \
	    cp -p "$$f" "$(_dstdir)/$$f"; \
	done
	@#
	@# Teach copy which version it is
	sed -i -e 's|^\(VERSION[ \t]*:\?=[ \t]*\).*|\1 $(VERSION)|' \
		"$(_dstdir)/Version.inc"
	find "$(_tmpdir)" -type f -exec $(_SUBSTITUTER_I) '{}' ';'
	@#
	@# Set $_ALL_SOURCES to real list of source files.
	sed -i -e 's|^\(_ALL_SOURCES[ \t]*:\?=[ \t]*\).*|\1 $(_ALL_SOURCES)|' \
		"$(_dstdir)/Makefile"
	@#
	@# Create RPM spec file, so it is directly available in the archive.
	$(MAKE) -C "$(_dstdir)" _packaging VERSION="$(VERSION)"
	@#
	(cd "$(_tmpdir)" && tar cf - *) | gzip -c >"$(_TARFILE)"
	rm -rf "$(_tmpdir)"
