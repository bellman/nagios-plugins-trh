#########################################################################
# Copyright © 2018   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


#########################################################################
# Version information
# This is a GNU Makefile calculating the version number, based on the
# released version (specified by the $_RELVERSION variable), and the
# status of the Git repository where this is run.
#
# - $_RELVERSION will be updated whenever a release is made.
# - $VERSION is calculated to add information about how far after the
#   released (tagged) version the Git repository is, and if it contains
#   uncommitted changes.


PKGNAME		:= nagios-plugins-trh

# The latest version released
_RELVERSION	:= 0.32

ifeq ($(shell git status >/dev/null 2>/dev/null; echo $$?),0)
_GITVERSION	:= $(shell \
	(git describe --tags --match='v$(_RELVERSION)' --abbrev=10 --dirty || \
	 printf 'v0.00-%d-g%s' `git log --oneline | wc -l` \
	                      `git describe --abbrev=10 --always --dirty` \
	) 2>/dev/null | sed -e 's/^v//' -e 'y/-/_/')
endif

VERSION := $(if $(_GITVERSION),$(_GITVERSION),$(_RELVERSION))


# Show the version information we have calculated, to help debugging
# the logic
.PHONY: _show_version
_show_version:
	@echo 'PKGNAME     = "$(PKGNAME)"'
	@echo '_RELVERSION = "$(_RELVERSION)"'
	@echo '_GITVERSION = "$(_GITVERSION)"'
	@echo 'VERSION     = "$(VERSION)"'
